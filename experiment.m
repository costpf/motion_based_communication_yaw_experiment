clear;clc;close all;
hostname = 'MSL-XPS-15-9550-2';
try rosinit(hostname);
catch
end
image_position = rossubscriber('/sender_image_position');
% Optitrack Initialization
frame = 'XY Plane';
opti = optitrackSetup(3000);

numsteps = 15; 
numpoints = 20; % Make sure this matches what's in the Codebook.
duration = 7;
load('Codebook.mat');
%Codebook = code_define_ellipses(numpoints);
true_class = 4;

noise_define(numpoints);

arr_true_Rot_b2w = zeros(3,3,numsteps);
arr_true_trans_b2w = zeros(3,numsteps);
arr_true_x = zeros(3*numpoints,numsteps);
arr_mu = cell(1,numsteps);
arr_Sigma = cell(1,numsteps);
arr_log_phi = zeros(length(Codebook),numsteps);
arr_class_entropy = zeros(1,numsteps);
arr_state_entropy = zeros(1,numsteps);
arr_z = zeros(2*numpoints,numsteps);
arr_time = 1:1:numsteps;
arr_u = zeros(4,numsteps-1);

% Take initial observation and Initialize belief.
fprintf('Step: 1\n');
satisfactory = false;
while satisfactory == false
    disp('Press r to observe.');
    while 1
        opti = readOptitrack(opti,frame);
        opti.pose(4:6,:) = rad2deg(opti.pose(4:6,:));
        u = [0;0;0;0];
        [true_Rot_b2w, true_trans_b2w, opti] = input_manual(opti,frame,u);
        %visualize_world_disp(opti,true_Rot_b2w,true_trans_b2w, false, [], [], [])
        visualize_world(true_Rot_b2w, true_trans_b2w, false, [], [], []);
        if strcmp(get(gcf, 'CurrentCharacter'), 'r')
            set(gcf,'CurrentCharacter', '@');
            break
        end
    end
    [success_flag, z, x] = observe_actual(image_position, duration, numpoints, opti, frame);
    if success_flag ~= 0
        disp('OK? [y/n]');
        while 1
            visualize_observation(z,numpoints)
            if strcmp(get(gcf,'CurrentCharacter'), 'y')
                set(gcf,'CurrentCharacter', '@');
                satisfactory = true;
                break;
            elseif strcmp(get(gcf,'CurrentCharacter'), 'n')
                set(gcf,'CurrentCharacter', '@');
                break;
            end
        end
    else 
        disp('Try again.');
    end
end
[mu, Sigma, log_phi] = prior_rob(z, Codebook);
[~,likely] = max(log_phi);
%visualize_world_disp(opti, true_Rot_b2w, true_trans_b2w, true, mu{likely}, Codebook{likely}, Codebook{true_class});
visualize_world(true_Rot_b2w, true_trans_b2w, true, mu{likely}, Codebook{likely}, x);
disp('Log of phi:');
disp(log_phi');
mixand_entropy = zeros(1,length(Codebook));
for ii = 1:length(Codebook)
    mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
end
state_entropy = dot(retrieve(log_phi), mixand_entropy);
class_entropy = -dot(retrieve(log_phi), log_phi);
disp('Class entropy:');
disp(class_entropy);
disp('State entropy:');
disp(state_entropy);
fprintf('\n');
arr_true_Rot_b2w(:,:,1) = true_Rot_b2w;
arr_true_trans_b2w(:,1) = true_trans_b2w;
arr_true_x(:,1) = x;
arr_mu{1} = mu;
arr_Sigma{1} = Sigma;
arr_log_phi(:,1) = log_phi;
arr_state_entropy(:,1) = state_entropy;
arr_class_entropy(:,1) = class_entropy;
arr_z(:,1) = z;

for timestep = 2:numsteps
    fprintf('Step: %d\n', timestep);
    u = control_state(log_phi, mu, Sigma, Codebook, numpoints);
    [true_Rot_b2w, true_trans_b2w, opti] = input_manual(opti, frame, u);
    [mu, Sigma, log_phi, z, x] = ekf_update(true_Rot_b2w, true_trans_b2w, mu, Sigma, log_phi, u, Codebook, numpoints, opti, frame, duration, image_position);
    [~,likely] = max(log_phi);
    %visualize_world_disp(opti, true_Rot_b2w, true_trans_b2w, true, mu{likely}, Codebook{likely}, Codebook{true_class});
    visualize_world(true_Rot_b2w, true_trans_b2w, true, mu{likely}, Codebook{likely}, x);
    disp('Log of phi:');
    disp(log_phi');
    mixand_entropy = zeros(1,length(Codebook));
    for ii = 1:length(Codebook)
        mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
    end
    state_entropy = dot(retrieve(log_phi), mixand_entropy);
    class_entropy = -dot(retrieve(log_phi), log_phi);
    disp('Class entropy:');
    disp(class_entropy);
    disp('State entropy:');
    disp(state_entropy);
    fprintf('\n');
    arr_true_Rot_b2w(:,:,timestep) = true_Rot_b2w;
    arr_true_trans_b2w(:,timestep) = true_trans_b2w;
    arr_true_x(:,timestep) = x;
    arr_mu{timestep} = mu;
    arr_Sigma{timestep} = Sigma;
    arr_log_phi(:,timestep) = log_phi;
    arr_state_entropy(:,timestep) = state_entropy;
    arr_class_entropy(:,timestep) = class_entropy;
    arr_z(:,timestep) = z;
    arr_u(:,timestep-1) = u;
    filename = sprintf('exp_result_class_%d.mat',true_class);
    save(filename, 'arr_true_Rot_b2w','arr_true_trans_b2w','arr_true_x','arr_mu','arr_Sigma','arr_log_phi','arr_state_entropy','arr_class_entropy','arr_z','arr_u','timestep','arr_time','numpoints','true_class');
end
