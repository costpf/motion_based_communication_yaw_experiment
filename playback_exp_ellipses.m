% ===================================
% FUNCTION [] = playback_exp_ellipses(filename)
% -----------
% DESCRIPTION:  Playback the belief state transitions and visualized
%               experimental results.
% INPUT:    filename    Filename of the generated experimental results file.
% -----------
% DEPENDENCIES: Codebook.mat, visualize_world(...), visualize_observation(...), retrieve(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function playback_exp_ellipses(filename)
load(filename);
load('Codebook.mat');
numpoints = length(Codebook{1})/3;

true_Rot_b2w = arr_true_Rot_b2w(:,:,1);
true_trans_b2w = arr_true_trans_b2w(:,1);
true_x = arr_true_x(:,1);
z = arr_z(:,1);
log_phi = arr_log_phi(:,1);
Sigma = arr_Sigma{1};
mu = arr_mu{1};

fprintf('Step: 1\n');
[~,likely] = max(log_phi);
visualize_observation(z,numpoints);
visualize_world(true_Rot_b2w, true_trans_b2w, true, mu{likely}, Codebook{likely}, true_x);
disp('Log of phi:');
disp(log_phi');
mixand_entropy = zeros(1,length(Codebook));
for ii = 1:length(Codebook)
    mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
end
state_entropy = arr_state_entropy(1);
class_entropy = arr_class_entropy(1);
disp('Class entropy:');
disp(class_entropy);
disp('State entropy:');
disp(state_entropy);
fprintf('\n');

for timestep = 2:length(arr_time)
    true_Rot_b2w = arr_true_Rot_b2w(:,:,timestep);
    true_trans_b2w = arr_true_trans_b2w(:,timestep);
    true_x = arr_true_x(:,timestep);
    z = arr_z(:,timestep);
    log_phi = arr_log_phi(:,timestep);
    Sigma = arr_Sigma{timestep};
    mu = arr_mu{timestep};
    
    fprintf('Step: %d\n', timestep);
    u = arr_u(:,timestep-1);
    disp('Control Input:');
    disp([u(1:3)', u(4)*180/pi]);
    disp('Log of phi:');
    disp(log_phi');
    [~,likely] = max(log_phi);
    mixand_entropy = zeros(1,length(Codebook));
    for ii = 1:length(Codebook)
        mixand_entropy(ii) = size(Sigma{ii},1)/2 + 1/2*logdet(2*pi*Sigma{ii});
    end
    state_entropy = arr_state_entropy(timestep);
    class_entropy = arr_class_entropy(timestep);
    disp('Class entropy:');
    disp(class_entropy);
    disp('State entropy:');
    disp(state_entropy);
    visualize_observation(z,numpoints);
    visualize_world(true_Rot_b2w, true_trans_b2w, true, mu{likely}, Codebook{likely}, true_x);
    fprintf('\n');
end
end