% ===================================
% FUNCTION [mu, Sigma, log_phi, z] = ekf_update(mu, Sigma, log_phi, u, Codebook, numpoints, opti, frame, duration, image_position)
% -----------
% DESCRIPTION:  Perform one-step MHEKF update for the belief state.
% INPUT:    true_Rot_b2w    Rotation matrix from body to world coordinates.
%           true_trans_b2w  Translation vector from body to world
%                           coordinates.
%           mu      Cellarray of state means.
%           Sigma   Cellarray of state covariances.
%           log_phi Log of multinomial parameters.
%           u       Control input (R^4).
%           Codebook Trajectory codebook.
%           numpoints   Number of points in one trajectory.
%           opti    Optitrack struct.
%           frame   frame definition.
%           duration    period of one trajectory.
%           image_position  ROS subscriber to /sender_image_position topic.
%
% OUTPUT:   mu      Cellarray of state means.
%           Sigma   Cellarray of state covariances.
%           log_phi Log of multinomial parameters.
%           z       Observation taken (R^(2*n)).
%           x       Performed trajectory (R^(3*n)).
%          
% -----------
% DEPENDENCIES: noise_param.mat, state_trans(...), observe_actual(...),
%               belief_propagate(...), observe(...), logdet(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
%   Haruki Nishimura    11-29-16    <-  true_r and true_x removed from the output args for
%                                       experiment.
%   Haruki Nishimura    12-04-16    <-  Lower bound for log_phi refined. x
%                                       added to the output.
% ===================================
function [mu, Sigma, log_phi, z, x] = ekf_update(true_Rot_b2w, true_trans_b2w, mu, Sigma, log_phi, u, Codebook, numpoints, opti, frame, duration, image_position)
load('noise_param.mat');


% Take a new observation.
satisfactory = false;
while satisfactory == false
    disp('Press r to observe.');
    while 1
        opti = readOptitrack(opti,frame);
        opti.pose(4:6,:) = rad2deg(opti.pose(4:6,:));
        %[true_Rot_b2w, true_trans_b2w, opti] = input_manual(opti,frame,u);
        visualize_world_disp(opti,true_Rot_b2w,true_trans_b2w, false, [], [], [])
        if strcmp(get(gcf, 'CurrentCharacter'), 'r')
            set(gcf,'CurrentCharacter', '@');
            break
        end
    end
    [success_flag, z, x] = observe_actual(image_position, duration, numpoints, opti, frame);
    if success_flag ~= 0
        disp('OK? [y/n]');
        while 1
            visualize_observation(z,numpoints)
            if strcmp(get(gcf,'CurrentCharacter'), 'y')
                set(gcf,'CurrentCharacter', '@');
                satisfactory = true;
                break;
            elseif strcmp(get(gcf,'CurrentCharacter'), 'n')
                set(gcf,'CurrentCharacter', '@');
                break;
            end
        end
    else 
        disp('Try again.');
    end
end

% MHEKF update.
for ii = 1:length(log_phi)
    % State transition estimation
    [r_bar,~,~,~,~,H,K,Sigma{ii}] = belief_propagate(mu{ii},Sigma{ii},u,Codebook,numpoints,ii);
    mu_bar = r_bar;
    z_est = observe(r_bar, Codebook{ii});
    innov = z - z_est;
    mu{ii} = mu_bar + K*innov;
    % Avoid singularities
    if norm(mu{ii}(1:3)) > pi
        mu{ii}(1:3) = (1 - 2*pi/norm(mu{ii}(1:3)))*mu{ii}(1:3);
    end
    log_phi(ii) = -1/2*logdet(2*pi*H) - 1/2*innov'*(H\innov) + log_phi(ii);
end
log_phi = log_phi - repmat(max(log_phi),length(Codebook),1);

% Prevent phi to become inaccurate due to computational limits.
for ii = 1:length(log_phi)
    if log_phi(ii) < -7.45e2
        log_phi(ii) = -7.45e2; %exp(-7.46e2) == 0, exp(-7.45e2) = 4.9407e-324
    end
end
end