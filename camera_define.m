% ===================================
% SCRIPT  camera_define.m
% -----------
% DESCRIPTION:  Define the intrinsic parameters of the camera and save as
%               camera_param.mat 
% OUTPUT:   camera_param.mat    A mat file containing camera calibration matrix.
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    10-06-16    <-  Created.
%   Haruki Nishimura    11-19-16    <-  Updated for major version up.
%   Haruki Nishimura    11-27-16    <-  [d_bc;0;0] is replaced with
%                                       [d_c2b_x;d_c2b_y;d_c2b_z] to allow
%                                       more flexiblity in the body-camera
%                                       configuration.
% ===================================

% Intrinsic parameters
% 5.14mm x3.50mm Image sensor, 800 x 600 pixels, f = 3.67mm (Logitech C920)

ku = 800/(5.14e-3); % pixel per unit length in u
kv = 600/(3.50e-3); % pixel per unit length in v
u0 = 800/2;   % principal point in u
v0 = 600/2;   % principal point in v
f = 3.67e-3;  % focul length
K = [ku*f, 0, u0;  % Camera calibration matrix
     0, kv*f, v0;
     0, 0, 1];
R_bc = [0, -1,  0; % Rotation matrix from the body coord. to the camera coord.
        0,  0, -1;
        1,  0,  0]; 
d_c2b_x = -0.01; % Position of the origin of the camera frame in the body frame (x-coord).
d_c2b_y = 0;     % Position of the origin of the camera frame in the body frame (y-coord).
d_c2b_z = -0.023;% Position of the origin of the camera frame in the body frame (z-coord).
save('camera_param.mat', 'K', 'R_bc', 'd_c2b_x', 'd_c2b_y', 'd_c2b_z');