function visualize_observation(z,numpoints)
load('camera_param.mat', 'K', 'R_bc');
u_max = 2*K(1,3);
v_max = 2*K(2,3);
clf(figure(1));
figure(1);
set(gcf, 'Position', [1050, 400, 0.5*u_max, 0.5*v_max]);
hold on;
set(gca,'Ydir','reverse');
for jj = 1:numpoints
    scatter(z(2*jj-1), z(2*jj), 'ro', 'linewidth',2.0);
    %scatter(z_hyp(2*jj-1), z_hyp(2*jj), 'bo', 'linewidth',2.0);
end
xlabel('u');
ylabel('v');
plot([0,u_max,u_max,0,0],[0,0,v_max,v_max,0],'m');
axis equal;
drawnow;
end