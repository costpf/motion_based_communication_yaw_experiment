function [success_flag, z, x] = observe_actual(subscriber, duration, numpoints, opti, frame)

index_sen = 2; % sender's index (optitrack)

disp('Making an Observation.');
U_acum = [];
V_acum = [];
x_acum = [];

tic;
success_flag = false;
detection_flag = false;
lost_flag = false;
detection_count = 10;
lost_count = 10;
while toc < duration + 5
   posdata = receive(subscriber,1);
   U = posdata.X;
   V = posdata.Y;
   if ~isnan(U)
       %fprintf('U = %.2f, V = %.2f\n', U,V);
       U_acum(end+1) = U;
       V_acum(end+1) = V;
       opti = readOptitrack(opti,frame);
       x_acum(:,end+1) = opti.pose(1:6,index_sen);
   end
   if ~isnan(U) && detection_flag == false
       first_detection_time = toc;
       detection_count = detection_count - 1;
       if detection_count == 0
           duration = duration + first_detection_time;
           detection_flag = true;
       end
   elseif isnan(U) && detection_flag == true
       lost_count = lost_count - 1;
       if lost_count == 0
           lost_flag = true;
       end
   end
   if toc > 1/2*duration && detection_flag == false
       disp('Sender Not Observed.');
       success_flag = false;
       z = [];
       x = [];
       break;
   elseif toc > duration && lost_flag == true
       disp('Observation Made.');
       success_flag = true;
       U_acum_resampled = interp1((0:1:length(U_acum)-1), U_acum, (0:1:numpoints-1).*(length(U_acum)-1)/numpoints);
       V_acum_resampled = interp1((0:1:length(V_acum)-1), V_acum, (0:1:numpoints-1).*(length(V_acum)-1)/numpoints);
       x1_acum_resampled = interp1((0:1:length(x_acum)-1), x_acum(1,:), (0:1:numpoints-1).*(length(x_acum)-1)/numpoints);
       x2_acum_resampled = interp1((0:1:length(x_acum)-1), x_acum(2,:), (0:1:numpoints-1).*(length(x_acum)-1)/numpoints);
       x3_acum_resampled = interp1((0:1:length(x_acum)-1), x_acum(3,:), (0:1:numpoints-1).*(length(x_acum)-1)/numpoints);
       z = reshape([U_acum_resampled;V_acum_resampled], 2*numpoints,1);
       x = reshape([x1_acum_resampled;x2_acum_resampled;x3_acum_resampled], 3*numpoints,1);
       %x = x_acum;
       break;
   elseif toc > duration && lost_flag == false
       disp('Time Out.');
       success_flag = false;
       z = [];
       x = [];
       break;
   end
end
end