% ===================================
% FUNCTION u = control_state(log_phi, mu, Sigma, Codebook, numpoints)
% -----------
% DESCRIPTION:  Choose control input according to the joint blief-state
%               entropy policy.
% INPUT:    log_phi Log of multinomial parameters.
%           mu      Cellarray of state means.
%           Sigma   Cellarray of state covariances.
%           Codebook Trajectory codebook.
%           numpoints   Number of points in one trajectory.
% OUTPUT:   u   Control input in R^4 that satisfies min(J1) subjet to J2 < (some threshold defined in this code).
% -----------
% DEPENDENCIES: J_state(...)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function u = control_state(log_phi, mu, Sigma, Codebook, numpoints)
tic;
trans_max = 0.5;
rot_max = 10*pi/180;
numdisc = 5;
space_yaw = linspace(-rot_max,rot_max,numdisc);
space_y = linspace(-trans_max,trans_max,numdisc);

inputs = zeros(4,numdisc);
costs = zeros(1,numdisc);

for ii = 1:numdisc;
    cost_for_y = zeros(1,numdisc);
    dx = 0;
    dz = 0;
    for jj = 1:numdisc
        dy = space_y(jj);
        inputs_temp = [dx;dy;dz;space_yaw(ii)];
        [~, cost_for_y(jj)] = feval(@J_state, log_phi, mu, Sigma, inputs_temp, Codebook, numpoints);
    end
    [min_cost_for_y,min_index_for_y] = min(cost_for_y);
    %disp(min_cost_for_y);
    if min_cost_for_y > 200^2
        costs(ii) = Inf;
    else
        inputs(:,ii) = [dx, space_y(min_index_for_y), dz, space_yaw(ii)];
        [costs(ii),~] = feval(@J_state, log_phi, mu, Sigma, inputs(:,ii), Codebook, numpoints);
    end
end
%disp(costs);
[~,minindex] = min(costs);
u = inputs(:, minindex);
disp('Control Input:');
disp([u(1:3)', u(4)*180/pi])

toc;
end