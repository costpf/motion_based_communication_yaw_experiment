
clear;clc;close all;
hostname = 'MSL-XPS-15-9550-2';
try rosinit(hostname);
catch
end
image_position = rossubscriber('/sender_image_position');
% Optitrack Initialization
frame = 'XY Plane';
opti = optitrackSetup(3000);

class = 4;
numpoints = 20;
duration = 6;
opt_id_stick = 2;


numrecs = 30;
data_raw = zeros(3,numpoints,numrecs);

for ii = 1:numrecs
    while 1
        fprintf('Record No. %d\n',ii);
        disp('Press r to begin recording');
        set(gcf,'CurrentCharacter', '@');
        while 1
            figure(1);
            plot(0,0);
            drawnow;
            if strcmp(get(gcf, 'CurrentCharacter'), 'r')
                set(gcf,'CurrentCharacter', '@');
                break
            end
        end
        
        tic;
        x_temp = [];
        y_temp = [];
        z_temp = [];
        detection_flag = false;
        lost_flag = false;
        lost_buffer = 20;
        satisfactory = false;
        while toc < duration
            posdata = receive(image_position,1);
            U = posdata.X;
            V = posdata.Y;
            if ~isnan(U)
                opti = readOptitrack(opti,frame);
                opti.pose(4:6,:) = rad2deg(opti.pose(4:6,:));
                x_temp(end+1) = opti.pose(1,opt_id_stick);
                y_temp(end+1) = opti.pose(2,opt_id_stick);
                z_temp(end+1) = opti.pose(3,opt_id_stick);
            end
            if isnan(U) && detection_flag == false
                duration = duration + toc;
            elseif ~isnan(U) && detection_flag == false
                duration = duration + toc;
                detection_flag = true;
            elseif isnan(U) && detection_flag == true
                lost_buffer = lost_buffer - 1;
                if lost_buffer == 0
                    lost_flag = true;
                    break;
                end
            end
        end
        if ~(length(x_temp) == 1 || length(y_temp) == 1 || length(z_temp) == 1)
            x_temp_resampled = interp1((0:1:length(x_temp)-1), x_temp(1,:), (0:1:numpoints-1).*(length(x_temp)-1)/numpoints);
            y_temp_resampled = interp1((0:1:length(y_temp)-1), y_temp(1,:), (0:1:numpoints-1).*(length(y_temp)-1)/numpoints);
            z_temp_resampled = interp1((0:1:length(x_temp)-1), z_temp(1,:), (0:1:numpoints-1).*(length(z_temp)-1)/numpoints);
            disp('OK? [y/n]');
            while 1
                figure(1);
                scatter3(x_temp_resampled,y_temp_resampled,z_temp_resampled);
                axis equal;
                drawnow;
                if strcmp(get(gcf,'CurrentCharacter'), 'y')
                    satisfactory = true;
                    set(gcf,'CurrentCharacter', '@');
                    break
                elseif strcmp(get(gcf,'CurrentCharacter'), 'n')
                    set(gcf,'CurrentCharacter', '@');
                    break
                end
            end
        end
       if satisfactory == false
           disp('Try again.');
       else
           data_raw(1,:,ii) = x_temp_resampled;
           data_raw(2,:,ii) = y_temp_resampled;
           data_raw(3,:,ii) = z_temp_resampled;
           break
       end
    end
end
% figure(1);
% hold off;
% scatter3(reshape(data_raw(1,:,:),1,numrecs*numpoints), reshape(data_raw(2,:,:),1,numrecs*numpoints), reshape(data_raw(3,:,:),1,numrecs*numpoints),'b');
% axis equal;
filename_raw = sprintf('code_%d_data_raw.mat', class);
save(filename_raw,'data_raw')


% Process data.
data = reshape(data_raw, 3, numpoints*numrecs);
mean_data = mean(data,2);
data = data - mean_data;
[U,S,V] = svd(data,'econ');
data = U'*data;
figure(1);
scatter3(data(1,:),data(2,:),data(3,:));
hold on;
axis equal;
plot3([0;1],[0;0],[0;0],'r');
plot3([0;0],[0;1],[0;0],'g');
plot3([0;0],[0;0],[0;1],'b');
data_concat = reshape(data, 3*numpoints, numrecs);
mean_concat = mean(data_concat, 2);
means = reshape(mean_concat, 3, numpoints);
figure(2);
scatter3(means(1,:),means(2,:),means(3,:));
axis equal;
Cov = cov(data_concat');

filename_processed = sprintf('code_%d_data_processed.mat', class);
save(filename_processed,'mean_concat','Cov');