function [true_Rot_b2w, true_trans_b2w, opti] = input_manual(opti, frame, u)
    index_rec = 1;
    
    % Read receiver's body pose from optitrack.
    rec_phi = opti.pose(4,index_rec);
    rec_theta = opti.pose(5,index_rec);
    rec_psi = opti.pose(6,index_rec);
    rec_tx = opti.pose(1,index_rec);
    rec_ty = opti.pose(2,index_rec);
    rec_tz = opti.pose(3,index_rec);
    % Map from optitrack frame to the world frame.
    Rot_opt2w = [0,1, 0;
        1,0, 0;
        0,0,-1];
    Rot_optb2b = [1, 0, 0;
        0,-1, 0;
        0, 0,-1];
    Rot_optb2opt = rotx(rec_phi)*roty(rec_theta)*rotz(rec_psi);
    true_Rot_b2w = Rot_opt2w*Rot_optb2opt*transpose(Rot_optb2b);
    true_trans_b2w = Rot_opt2w*[rec_tx;rec_ty;rec_tz];

    desired_body_Rot = Rot_opt2w'*true_Rot_b2w*expm(skew([0;0;u(4)]))*Rot_optb2b;
    desired_body_trans = Rot_opt2w'*(true_Rot_b2w*u(1:3) + true_trans_b2w);
    rec_euler_new = rot2ZYXeuler(desired_body_Rot);
    desired_body_pose = [desired_body_trans; rad2deg(rec_euler_new)];
    
    if ~isequal(u, [0;0;0;0])
        disp('Move the receiver and press r when ready.')
        pause(1);
        opti_new = opti;
        
        while 1
            opti_new = readOptitrack(opti_new, frame);
            opti_new.pose(4:6,:) = rad2deg(opti_new.pose(4:6,:));
            
            % Read receiver's body pose from optitrack.
            rec_phi = opti_new.pose(4,index_rec);
            rec_theta = opti_new.pose(5,index_rec);
            rec_psi = opti_new.pose(6,index_rec);
            rec_tx = opti_new.pose(1,index_rec);
            rec_ty = opti_new.pose(2,index_rec);
            rec_tz = opti_new.pose(3,index_rec);
            % Map from optitrack frame to the world frame.
            Rot_opt2w = [0,1, 0;
                1,0, 0;
                0,0,-1];
            Rot_optb2b = [1, 0, 0;
                0,-1, 0;
                0, 0,-1];
            Rot_optb2opt = rotx(rec_phi)*roty(rec_theta)*rotz(rec_psi);
            true_Rot_b2w = Rot_opt2w*Rot_optb2opt*transpose(Rot_optb2b);
            true_trans_b2w = Rot_opt2w*[rec_tx;rec_ty;rec_tz];
            
            current_body_pose = opti_new.pose(:,index_rec);
            disp([round(current_body_pose,3), round(desired_body_pose,3)]);
            
            visualize_world_disp(opti_new,true_Rot_b2w,true_trans_b2w, false, [], [], []);
            
            if strcmp(get(gcf, 'CurrentCharacter'), 'r')
                disp('New Body Pose Defined.');
                set(gcf, 'CurrentCharacter', '@');
                break;
            end
        end
        opti = opti_new;
    end
end
