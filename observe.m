% ===================================
% FUNCTION z = observe(r,x)
% -----------
% DESCRIPTION:  Observation model without noise.
% INPUT:    r   Receiver's state (R^6).
%           x   Trajectory (R^(3*n)).
% OUTPUT:   z   Observation (R^(2*n)) 
% DEPENDENCIES: proj_mat(r)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function z = observe(r,x)
numpoints = length(x)/3;
P = proj_mat(r);
x = reshape(x,3,numpoints);
z = zeros(2,numpoints);
for ii = 1:numpoints
    proj = P*[x(:,ii);1];
    u = proj(1)/proj(3);
    v = proj(2)/proj(3);
    z(:,ii) = [u;v];
end
z = reshape(z,2*numpoints,1);
end