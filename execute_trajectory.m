function execute_trajectory(numpoints_sender, true_class)
Codebook = code_define_ellipses(numpoints_sender);
Positions = reshape(Codebook{true_class}, 3, numpoints_sender);
Xs = Positions(1,:);
Ys = Positions(3,:);
figure(1);
for ii = 1:numpoints_sender
    plot(Xs,Ys); hold on;
    scatter(Xs(ii),Ys(ii),12000,[0.8,0,0],'filled');
    xlim([-2.5,2.5]);
    ylim([-3.5, 0.5]);
    axis equal;
    drawnow;
    pause(0.01);
    hold off;
end
