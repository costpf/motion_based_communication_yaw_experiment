clear;clc;close all;
hostname = 'MSL-XPS-15-9550-2';
try rosinit(hostname);
catch
end
position = rossubscriber('/sender_image_position');
% Optitrack Initialization
frame = 'XY Plane';
opti = optitrackSetup(3000);

opti = readOptitrack(opti,frame);
opti.pose(4:6,:) = rad2deg(opti.pose(4:6,:));
u = [0;0;0;0];
[true_Rot_b2w, true_trans_b2w, opti] = input_manual(opti,frame,u);

visualize_world_disp(opti,true_Rot_b2w,true_trans_b2w, false, [], [], [])

    