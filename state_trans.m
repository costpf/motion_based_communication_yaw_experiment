% ===================================
% FUNCTION r = state_trans(r,u)
% -----------
% DESCRIPTION: State transition model without noise.
% INPUT:    r   Current state of the receiver in R^6.
%           u   New control input in R^4.
% OUTPUT:   r   New State without noise.
% -----------
% DEPENDENCIES: skew(x), inv_skew(X)
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
% ===================================
function r = state_trans(r,u)
w = r(1:3);
t = r(4:6);
t = t + expm(skew(w))*[u(1);u(2);u(3)];
w = inv_skew(logm(expm(skew(w))*expm(skew([0;0;u(4)]))));
% Avoid singularities.
if norm(w) > pi;
    w = (1 - 2*pi/norm(w))*w;
end
r = [w;t];
end