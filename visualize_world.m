function visualize_world(true_Rot_b2w,true_trans_b2w, draw_est_flag, est_r, est_x, true_x)
load('camera_param.mat', 'K', 'R_bc', 'd_c2b_x', 'd_c2b_y', 'd_c2b_z');


% Map from optitrack frame to the world frame.
Rot_opt2w = [0,1, 0;
           1,0, 0;
           0,0,-1];
%{
Rot_optdisp2traj = [1, 0, 0;
                  0,-1, 0;
                  0, 0,-1];
Rot_optdisp2opt = rotx(disp_phi)*roty(disp_theta)*rotz(disp_psi);
true_Rot_traj2w = Rot_opt2w*Rot_optdisp2opt*transpose(Rot_optdisp2traj);
true_trans_traj2w = Rot_opt2w*[disp_tx;disp_ty;disp_tz];
%}

% Calculate camera pose in the world coordinates.(external parameters matrix with [R|t])
true_camera_rot = true_Rot_b2w*R_bc';
true_camera_trans = true_trans_b2w + true_Rot_b2w*[d_c2b_x;d_c2b_y;d_c2b_z];
true_camera_pose = [true_camera_rot, true_camera_trans];

clf(figure(2));
figure(2);
set(gcf, 'Position', [100, 100, 900, 700]);
grid on; hold on;
% draw world axes.
line([0,0.4],[0,0],[0,0],'Linestyle','-','Color','r');
line([0,0],[0,0.4],[0,0],'Linestyle','-','Color','g');
line([0,0],[0,0],[0,0.4],'Linestyle','-','Color','b');
xlabel('X_w'); ylabel('Y_w'); zlabel('Z_w');
%{
% draw true trajectory axes.
e1_traj = true_Rot_traj2w*[0.2;0;0] + true_trans_traj2w;
e2_traj = true_Rot_traj2w*[0;0.2;0] + true_trans_traj2w;
e3_traj = true_Rot_traj2w*[0;0;0.2] + true_trans_traj2w;
line([true_trans_traj2w(1),e1_traj(1)],[true_trans_traj2w(2),e1_traj(2)],[true_trans_traj2w(3),e1_traj(3)],'Linestyle','-','Color','r');
line([true_trans_traj2w(1),e2_traj(1)],[true_trans_traj2w(2),e2_traj(2)],[true_trans_traj2w(3),e2_traj(3)],'Linestyle','-','Color','g');
line([true_trans_traj2w(1),e3_traj(1)],[true_trans_traj2w(2),e3_traj(2)],[true_trans_traj2w(3),e3_traj(3)],'Linestyle','-','Color','b');
%}
% draw body axes.
e1_body = true_Rot_b2w*[0.1;0;0] + true_trans_b2w;
e2_body = true_Rot_b2w*[0;0.1;0] + true_trans_b2w;
e3_body = true_Rot_b2w*[0;0;0.1] + true_trans_b2w;
line([true_trans_b2w(1),e1_body(1)],[true_trans_b2w(2),e1_body(2)],[true_trans_b2w(3),e1_body(3)],'Linestyle','-','Color','r');
line([true_trans_b2w(1),e2_body(1)],[true_trans_b2w(2),e2_body(2)],[true_trans_b2w(3),e2_body(3)],'Linestyle','-','Color','g');
line([true_trans_b2w(1),e3_body(1)],[true_trans_b2w(2),e3_body(2)],[true_trans_b2w(3),e3_body(3)],'Linestyle','-','Color','b');

scatter3(true_trans_b2w(1),true_trans_b2w(2),true_trans_b2w(3),'r');
plotCamera('Location',true_camera_pose(:,4)','Orientation',transpose(true_camera_pose(:,1:3)),'Opacity',0, 'Size',0.1);

axis equal;
view(26,9);
%xlim([-1,3]);
%ylim([-1,3]);
%zlim([0,3]);

% draw estimated trajectory axes.
if draw_est_flag == true
    est_Rot_traj2w = true_Rot_b2w*transpose(expm(skew(est_r(1:3))));
    est_trans_traj2w = true_trans_b2w - est_Rot_traj2w*est_r(4:6);
    
    e1_traj_est = est_Rot_traj2w*[0.2;0;0] + est_trans_traj2w;
    e2_traj_est = est_Rot_traj2w*[0;0.2;0] + est_trans_traj2w;
    e3_traj_est = est_Rot_traj2w*[0;0;0.2] + est_trans_traj2w;
    line([est_trans_traj2w(1),e1_traj_est(1)],[est_trans_traj2w(2),e1_traj_est(2)],[est_trans_traj2w(3),e1_traj_est(3)],'Linestyle','-.','Color','r');
    line([est_trans_traj2w(1),e2_traj_est(1)],[est_trans_traj2w(2),e2_traj_est(2)],[est_trans_traj2w(3),e2_traj_est(3)],'Linestyle','-.','Color','g');
    line([est_trans_traj2w(1),e3_traj_est(1)],[est_trans_traj2w(2),e3_traj_est(2)],[est_trans_traj2w(3),e3_traj_est(3)],'Linestyle','-.','Color','b');
    
    numpoints = length(true_x)/3;
    for ii = 1:numpoints
        % True trajectory class with true pose.
        %true_x(3*ii-2:3*ii) = true_Rot_traj2w*true_x(3*ii-2:3*ii) + true_trans_traj2w;
        true_x(3*ii-2:3*ii) = Rot_opt2w*true_x(3*ii-2:3*ii);
        % Estimated trajectory class with estimated pose.
        est_x(3*ii-2:3*ii) = est_Rot_traj2w*est_x(3*ii-2:3*ii) + est_trans_traj2w;
    end
    h1 = cell(1,numpoints);
    h2 = cell(1,numpoints);
    for jj = 1:numpoints
        h1{jj} = scatter3(true_x(3*jj-2),true_x(3*jj-1),true_x(3*jj),'r');
        h2{jj} = scatter3(est_x(3*jj-2),est_x(3*jj-1),est_x(3*jj),'b');
        drawnow;
        pause(1/numpoints);
    end
end

drawnow;
end