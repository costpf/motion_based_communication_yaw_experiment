% ===================================
% FUNCTION [] = noise_define(numpoints)
% -----------
% DESCRIPTION:  Define noise parameters and save as noise_param.mat.
% INPUT:    numpoints   Number of points in one trajectory.
% OUTPUT:   noise_param.mat
% DEPENDENCIES: Codebook.mat
% -----------
% REVISION HISTORY:
%   Haruki Nishimura    11-19-16    <-  Created.
%   Haruki Nishimura    12-04-16    <-  Modified for experiments only to
%                                       load the trajectory generation
%                                       covariance matrices from Codebook.mat.
% ===================================
function noise_define(numpoints)
load('Codebook.mat');
if numpoints ~= numpoints_code
    disp('Error: numpoints changed from Codebook.mat.');
end
% Noise Covariance Parameters
Qw_new = 0.01*eye(3); % Receiver's rotational noise (3-by-3)
Qt_new = 0.05*eye(3)*eye(3); % Receiver's translational noise (3-by-3)
%S_new = 0.1*speye(3*numpoints); % Trajectory noise (3n-by-3n)
R_new = 5*speye(2*numpoints); % Observation noise (2n-by-2n)
S_new = S_code;

Q_new = [Qw_new,  zeros(3);
         zeros(3), Qt_new];
filename = 'noise_param.mat';
if exist(filename, 'file')
    load(filename);
    if ~(isequal(Q, Q_new) && isequal(S, S_new) && isequal(R, R_new))
        Q = Q_new;
        S = S_new;
        R = R_new;
        save(filename, 'Q','S','R');
    end
else
    Q = Q_new;
    S = S_new;
    R = R_new;
    save(filename, 'Q','S','R');
end
end